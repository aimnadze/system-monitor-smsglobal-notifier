#!/bin/bash
cd `dirname $BASH_SOURCE`
./stop.sh
node program.js > program.out 2> program.err &
echo $! > program.pid
sleep 0.2
cat program.out program.err
