function sendNewSms (text) {
    smsNumber++
    sendSms(text, smsNumber)
}

function sendSms (text, smsNumber) {

    if (verbose) {
        console.log('Sending SMS #' + smsNumber + ' ' + JSON.stringify(text) + ' ...')
    }

    var options = {
        hostname: 'www.smsglobal.com',
        path: '/http-api.php?' + querystring.stringify({
            action: 'sendsms',
            user: smsGlobal.user,
            password: smsGlobal.password,
            from: smsGlobal.from,
            to: smsGlobal.to,
            text: text
        }),
    }

    var req = https.get(options, function (res) {
        if (verbose) {
            console.log('SMS #' + smsNumber + ' ' + JSON.stringify(text) + ' sent.')
        }
        clearTimeout(abortTimeout)
        req.abort()
    })
    req.on('error', function (err) {
        if (verbose) {
            console.log('Sending SMS #' + smsNumber + ' ' + JSON.stringify(text) + ' failed.')
            console.log('Sending SMS #' + smsNumber + ' ' + JSON.stringify(text) + ' scheduled.')
        }
        clearTimeout(abortTimeout)
        setTimeout(function () {
            sendSms(text, smsNumber)
        }, 1000)
    })

    var abortTimeout = setTimeout(function () {
        if (verbose) {
            console.log('Sending SMS #' + smsNumber + ' ' + JSON.stringify(text) + ' timed out.')
        }
        req.abort()
    }, 1000 * 20)

}

var config = require('./config.js'),
    http = require('http'),
    https = require('https'),
    querystring = require('querystring'),
    url = require('url')

var smsGlobal = config.smsGlobal,
    verbose = config.verbose,
    smsNumber = 0

var nodes = config.nodes
for (var i in nodes) {
    (function (node) {

        function check () {

            if (verbose) console.log('Checking ' + name + ' ...')

            var req = http.get(options, function (res) {
                if (verbose) console.log('Checking ' + name + ' succeeded.')
                clearTimeout(abortTimeout)
                var statusCode = res.statusCode
                req.abort()
                if (statusCode === 200) lastWasError = false
                else {
                    if (!lastWasError) {
                        lastWasError = true
                        sendNewSms(name + ': http status code: ' + statusCode)
                    }
                }
                scheduleCheck()
            })
            req.on('error', function (err) {
                if (verbose) console.log('Checking ' + name + ' failed.')
                clearTimeout(abortTimeout)
                if (!lastWasError) {
                    lastWasError = true
                    sendNewSms(name + ': network error: ' + err.code)
                }
                scheduleCheck()
            })

            var abortTimeout = setTimeout(function () {
                if (verbose) console.log('Checking ' + name + ' timed out.')
                req.abort()
            }, 1000 * 30)

        }

        function scheduleCheck () {
            if (verbose) console.log('Checking ' + name + ' scheduled.')
            setTimeout(check, 1000 * 60)
        }

        var lastWasError = false,
            name = node.name

        var options = {
            hostname: node.host,
            port: node.port,
            path: '/' + node.key,
            headers: {
                host: node.hostHeader,
            }
        }

        check()

    })(nodes[i])
}

var pages = config.pages
for (var i in pages) {
    (function (page) {

        function check () {

            if (verbose) console.log('Checking ' + page + '...')

            var req = client.get(options, function (res) {
                if (verbose) console.log('Checking ' + page + ' succeeded.')
                clearTimeout(abortTimeout)
                var statusCode = res.statusCode
                req.abort()
                if (statusCode === 200) lastWasError = false
                else {
                    if (!lastWasError) {
                        lastWasError = true
                        sendNewSms(page + ': http status code: ' + statusCode)
                    }
                }
                scheduleCheck()
            })
            req.on('error', function (err) {
                if (verbose) console.log('Checking ' + page + ' failed.')
                clearTimeout(abortTimeout)
                if (!lastWasError) {
                    lastWasError = true
                    sendNewSms(page + ': network error: ' + err.code)
                }
                scheduleCheck()
            })

            var abortTimeout = setTimeout(function () {
                if (verbose) console.log('Checking ' + page + ' timed out.')
                req.abort()
            }, 1000 * 30)

        }

        function scheduleCheck () {
            if (verbose) console.log('Checking ' + page + ' scheduled.')
            setTimeout(check, 1000 * 60)
        }

        var lastWasError = false
        var options = url.parse(page)
        var client = options.protocol === 'https:' ? https : http

        check()

    })(pages[i])
}
