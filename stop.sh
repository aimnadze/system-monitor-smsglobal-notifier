#!/bin/bash
cd `dirname $BASH_SOURCE`
if [ -f program.pid ]
then
    pid=`cat program.pid`
    if ps -p $pid > /dev/null; then
        kill $pid
        while ps -p $pid > /dev/null; do
            sleep 0.2
        done
    fi
    ./clean.sh
fi
