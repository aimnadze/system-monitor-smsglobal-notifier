System Monitor SMSGlobal Notifier
=================================

A program to notify about failures of running system monitor nodes
and/or other web pages via SMS using [SMSGlobal](https://www.smsglobal.com/).

Scripts
-------

* `./restart.sh` - start/restart the program.
* `./stop.sh` - stop the program.
* `./clean.sh` - clean the program after an unexpected shutdown.

Configuration
-------------

`config.js` contains the configuration. See `example-config.js` for details.

See Also
--------

* [System Monitor Node](https://gitlab.com/aimnadze/system-monitor-node)
* [System Monitor Viewer](https://gitlab.com/aimnadze/system-monitor-viewer)
