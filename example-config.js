exports.verbose = true

// Example:
// exports.smsGlobal = {
//     user: 'my_user',
//     password: '_mySecret512P',
//     from: '+0123456789',
//     to: '+1234567890',
// }
exports.smsGlobal = {
    user: '',
    password: '',
    from: '',
    to: '',
}

// Example:
// exports.nodes = [
//     {
//         name: 'local-node',
//         host: '127.0.0.1',
//         port: 7074,
//         hostHeader: 'system-monitor-node',
//         key: 'jlIuF0043C3RslaOFbGc20Z1CfFke113',
//     },
//     ...
// ]
exports.nodes = []

// Example:
// exports.pages = [
//     'http://example.com/',
//     'http://another.example.com/',
//     ...
// ]
exports.pages = []
